var scene = new THREE.Scene(); 
var renderer = new THREE.WebGLRenderer(); renderer.setSize(window.innerWidth, window.innerHeight); 
document.body.appendChild(renderer.domElement);  

// set up world
wallWidth = 5;
wallHeight = 2;
wallDepth = 0.3;

var wallGeometry = new THREE.CubeGeometry(wallWidth,wallHeight,wallDepth,1,1,1); 
var wallTexture = new THREE.ImageUtils.loadTexture('assets/scenery/texture_brick1.jpg');
wallTexture.wrapS = THREE.RepeatWrapping;
wallTexture.wrapT = THREE.RepeatWrapping;
wallTexture.repeat.x = wallWidth;
wallTexture.repeat.y = wallHeight;
wallTexture.needsUpdate = true;
var wallMaterial = new THREE.MeshBasicMaterial({map: wallTexture});
var wall = new THREE.Mesh(wallGeometry, wallMaterial);
wall.position.y = wallHeight/2.0;
//wall.rotation = {x:90 * Math.PI / 180,y:0,z:0};


var floorGeometry = new THREE.PlaneGeometry(20,20,1,1); 
var floorTexture = new THREE.ImageUtils.loadTexture('assets/scenery/texture_grass1.jpg');
floorTexture.wrapS = THREE.RepeatWrapping;
floorTexture.wrapT = THREE.RepeatWrapping;
floorTexture.repeat = new THREE.Vector2(20,20);
var floorMaterial = new THREE.MeshBasicMaterial({map: floorTexture});
var floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.rotation = {x:-90* Math.PI / 180,z:0,y:0};

var grumpyGeometry = new THREE.PlaneGeometry(0.3,0.3,1,1); 
var grumpyTexture = new THREE.ImageUtils.loadTexture('assets/sprays/grumpycat.png');
var grumpyMaterial = new THREE.MeshBasicMaterial({map: grumpyTexture, transparent: true});
var grumpy = new THREE.Mesh(grumpyGeometry, grumpyMaterial);
grumpy.rotation.x = 0* Math.PI;
grumpy.rotation.y = 180* Math.PI;

scene.add(wall);
scene.add(floor);
scene.add(grumpy);

var objects = [wall];

// marker
//var markerGeometry = new THREE.SphereGeometry(0.5); 
//var markerMaterial = new THREE.MeshBasicMaterial({color: 0x00ff00});
//var marker = new THREE.Mesh(markerGeometry, markerMaterial);
//scene.add(marker);

// set up camera
var camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight, 0.1, 1000); 
camera.position = {x:3,y:1.6,z:-5};
camera.lookAt(new THREE.Vector3(0,1.6,0));

//.. and its controls
var controls = new THREE.FirstPersonControls( camera );

controls.movementSpeed = 70;
controls.lookSpeed = 0.01;
controls.noFly = true;
controls.lookVertical = true;

var clock = new THREE.Clock();

var projector = new THREE.Projector();
	
function spray( event ) {
	var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );
	projector.unprojectVector( vector, camera );

	var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

	var intersects = raycaster.intersectObjects( objects );
	
	console.log(intersects[0].point);

	if ( intersects.length > 0 ) {
		grumpy.position = intersects[ 0 ].point;
		grumpy.position.z -= 0.1;
	}
	
	
}	

function colour( event ) {
	var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );
	projector.unprojectVector( vector, camera );

	var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

	var intersects = raycaster.intersectObjects( objects );

	if ( intersects.length > 0 ) {
		intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff );
	}
}

function mouseDownHandler( event ){
	switch(event.which){
		case 1:
			// Left Click
			spray(event);
			break;
		case 3:
			// Right Click
			colour(event);
			break;
	}
}

document.addEventListener( 'mousedown', mouseDownHandler, false );

function render() {
	renderer.render(scene, camera); 
	var delta = clock.getDelta();
	requestAnimationFrame(render);
	grumpy.rotation.x = 3.2; 
	grumpy.rotation.z = 3; 
	
	//controls.update(delta);
	
}

render();

